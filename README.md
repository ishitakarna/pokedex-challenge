## Setup

Requires node version `16.17.0`

```
npm i
npm start
```

After installing dependencies and starting the dev server, access the project at `localhost:8080`.

## Challenge

### Background

When you access the application in your web browser, you will see a grid view of pokemon on the
home page. After clicking on one of the pokemon, you can "capture" it and view your captured
pokemon by selecting the "Captured Pokemons" button in the header.

### Task

While the application currently allows you to capture pokemon, there is no way to "release" them
so they no longer show up on the "Captured Pokemons" page.

Your task is to add a button to each row of the "Captured Pokemons" page (http://localhost:8080/captured)
which allows the user to "release" the pokemon so they no longer show on this page.

Please fork this repository, and create a pull request with your changes so we can discuss them later!

Feel free to use any resources/documentation you need to complete the task!
