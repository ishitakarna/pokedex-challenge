import { combineReducers } from 'redux';

import capturedPokemons from './capturedPokemons';
import pokemons from './pokemons';

export default combineReducers({
  capturedPokemons,
  pokemons
});
