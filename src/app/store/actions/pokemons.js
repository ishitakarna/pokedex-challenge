import _ from 'lodash';

export const FETCH_POKEMONS = 'fetch-pokemons';
export const FETCH_POKEMONS_DONE = 'fetch-pokemons-done';
export const LOAD_POKEMONS = 'load-pokemons';
export const LOAD_POKEMON = 'load-pokemon';

export const fetchPokemons = () => {
  return (dispatch, getState) => {
    const state = getState();
    if (!state.pokemons.loading && state.pokemons.nextPageUrl) {
      dispatch({
        type: FETCH_POKEMONS
      });
      return fetch(state.pokemons.nextPageUrl).then((response) => {
        if (response.status === 200) {
          return response.json().then(({ next, results }) => {
            return Promise.all(
              _.map(results, (result) => dispatch(fetchPokemon({ url: result.url })))
            ).then((pokemons) => {
              dispatch({
                pokemons,
                type: LOAD_POKEMONS,
                nextPageUrl: next
              });
            });
          });
        } else {
          return Promise.reject(new Error(response.status));
        }
      }).finally(() => {
        dispatch({
          type: FETCH_POKEMONS_DONE
        });
      });
    }
  };
};

export const fetchPokemon = ({ id: _id, url }) => {
  const id = _id || _.last(_.compact(url.split('/')));
  return (dispatch, getState) => {
    const state = getState();
    const pokemon = state.pokemons.byId[id];
    if (pokemon) {
      return Promise.resolve(pokemon);
    } else {
      return fetch(`https://pokeapi.co/api/v2/pokemon/${id}/`).then((response) => {
        if (response.status === 200) {
          return response.json().then((pokemon) => {
            dispatch({
              pokemon,
              type: LOAD_POKEMON
            })
            return pokemon;
          });
        } else {
          return Promise.reject(new Error(response.status));
        }
      });
    }
  };
};
