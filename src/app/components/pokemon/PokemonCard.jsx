import classNames from 'classnames';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';

import { ANIMATION_DELAY } from 'app/components/App';
import { PokemonPropType } from 'app/components/pokemon';
import PokemonName from 'app/components/pokemon/PokemonName';

import styles from './pokemon-card.scss';

const PokemonCard = (props) => {
  const [hide, setHide] = useState(props.delay);

  useEffect(() => {
    _.delay(() => {
      setHide(false);
    }, ANIMATION_DELAY);
  }, []);

  if (hide) {
    return null;
  }

  return (
    <div
      className={
        classNames(styles.pokemonCard, {
          [styles.clickable]: Boolean(props.onClick)
        })
      }
      onClick={ props.onClick && (() => props.onClick(props.pokemon)) }
    >
      <div
        className={
          classNames(
            styles.imageBackground,
            styles[props.pokemon.types[0].type.name]
          )
        }
      >
        <img
          className={ styles.image }
          src={ props.pokemon.sprites.other['official-artwork'].front_default }
        />
      </div>
      <div className={ styles.details }>
        <div className={ styles.name }>
          <PokemonName
            pokemon={ props.pokemon }
          />
        </div>
        <div className={ styles.types }>
          {
            _.map(props.pokemon.types, ({ type }) => (
              <div
                key={ type.name }
                className={ styles.type }
              >
                { _.capitalize(type.name) }
              </div>
            ))
          }
        </div>
      </div>
    </div>
  );
};

PokemonCard.propTypes = {
  delay: PropTypes.bool,
  pokemon: PokemonPropType.isRequired,
  onClick: PropTypes.func
};

export default PokemonCard;
