import classNames from 'classnames';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import FormattedDate from 'lib/components/wrappers/datetime/FormattedDate';
import Button from 'lib/components/inputs/button/Button';

import { CapturedPokemonPropType } from 'app/components/pokemon';
import PokemonName from 'app/components/pokemon/PokemonName';

import styles from './captured-pokemon-grid.scss';
import { releasePokemon, loadCapturedPokemons, saveCapturedPokemons } from '../../store/actions/capturedPokemons';

const CapturedPokemonRow = (props) => {
  const pokemon = props.pokemonsById[props.capturedPokemon.pokemonId];
  console.log(props)

  function releasePokemon() {
    props.releasePokemon(pokemon.id);
    props.saveCapturedPokemons();
    props.loadCapturedPokemons();
  }

  return (
    <div
      className={ styles.row }
    >
      <div
        className={ styles.pokemon }
      >
        <div
          className={
            classNames(
              styles.imageBackground,
              styles[pokemon.types[0].type.name]
            )
          }
        >
          <img
            className={ styles.image }
            src={ pokemon.sprites.other['official-artwork'].front_default }
          />
        </div>
        <div className={ styles.details }>
          <div className={ styles.name }>
            <PokemonName
              pokemon={ pokemon }
            />
          </div>
          <div className={ styles.types }>
            {
              _.map(pokemon.types, ({ type }) => (
                <div
                  key={ type.name }
                  className={ styles.type }
                >
                  { _.capitalize(type.name) }
                </div>
              ))
            }
          </div>
        </div>
      </div>
      <div
        className={
          classNames(styles.nickname, {
            [styles.none]: !props.capturedPokemon.nickname
          })
        }
      >
        { props.capturedPokemon.nickname || 'None' }
      </div>
      <div
        className={ styles.capturedDate }
      >
        <FormattedDate
          date={ props.capturedPokemon.capturedDate }
        />
      </div>
      <div
        className={ styles.capturedLevel }
      >
        { props.capturedPokemon.capturedLevel }
      </div>
      <div
        className={ styles.releaseButton }
      >
        <Button grow primary onClick = {releasePokemon}>
              {'Release'}
        </Button>
      </div>
    </div>
  );
};

CapturedPokemonRow.propTypes = {
  pokemonsById: PropTypes.object.isRequired,
  capturedPokemon: CapturedPokemonPropType.isRequired
};

export default connect(
  (state) => ({
    pokemonsById: state.pokemons.byId
  }),
  {
    releasePokemon,
    loadCapturedPokemons,
    saveCapturedPokemons
  }
)(CapturedPokemonRow);
