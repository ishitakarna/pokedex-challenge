import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

import { fetchPokemon } from 'app/store/actions/pokemons';
import PokedexPage from 'app/components/pokedex/PokedexPage';
import { CapturedPokemonPropType } from 'app/components/pokemon';
import CapturedPokemonGrid from 'app/components/pokemon/CapturedPokemonGrid';
import PokeballSpinner from 'app/components/pokemon/PokeballSpinner';

const CapturedPokemonIndexPage = (props) => {
  const getInitialized = () => (
    _.some(props.capturedPokemons) &&
    _.every(props.capturedPokemons, (capturedPokemon) => (
      props.pokemonsById[capturedPokemon.pokemonId]
    ))
  );
  const [initialized, setInitialized] = useState(getInitialized);
  const [delayGrid] = useState(() => !getInitialized());

  function fetchPokemons() {
    const pokemonIds = _.uniq(
      _.map(props.capturedPokemons, 'pokemonId')
    );
    return Promise.all(
      _.map(
        pokemonIds,
        (pokemonId) => props.fetchPokemon({ id: pokemonId })
      )
    );
  }

  useEffect(() => {
    fetchPokemons().then(() => {
      setInitialized(true);
    });
  }, []);

  return (
    <PokedexPage>
      {
        initialized && (
          <CapturedPokemonGrid
            delay={ delayGrid }
            capturedPokemons={ props.capturedPokemons }
          />
        )
      }
      <PokeballSpinner
        grow
        hide={ initialized }
      />
    </PokedexPage>
  );
};

CapturedPokemonIndexPage.propTypes = {
  pokemonsById: PropTypes.object.isRequired,
  capturedPokemons: PropTypes.arrayOf(
    CapturedPokemonPropType
  ).isRequired,
  fetchPokemon: PropTypes.func.isRequired
};

export default connect(
  (state) => ({
    pokemonsById: state.pokemons.byId,
    capturedPokemons: state.capturedPokemons.all
  }),
  {
    fetchPokemon
  }
)(CapturedPokemonIndexPage);
