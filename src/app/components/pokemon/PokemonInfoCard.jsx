import classNames from 'classnames';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { connect } from 'react-redux';

import Modal from 'lib/components/wrappers/modal/Modal';
import FormattedDate from 'lib/components/wrappers/datetime/FormattedDate';
import Button from 'lib/components/inputs/button/Button';

import { PokemonPropType, CapturedPokemonPropType } from 'app/components/pokemon';
import PokemonCaptureForm from 'app/components/pokemon/PokemonCaptureForm';
import PokemonName from 'app/components/pokemon/PokemonName';

import styles from './pokemon-info-card.scss';

const BASE_STAT_LABELS = {
  'hp': 'HP',
  'attack': 'Attack',
  'defense': 'Defense',
  'special-attack': 'Special Attack',
  'special-defense': 'Special Defense',
  'speed': 'Speed'
};

const PokemonInfoCard = (props) => {
  const [showCaptureForm, setShowCaptureForm] = useState(false);
  const capturedPokemon = _.find(props.capturedPokemons, {
    pokemonId: props.pokemon.id
  });

  function capturePokemon(capturedPokemon) {
    if (props.onCapture) {
      props.onCapture(capturedPokemon);
    }
    setShowCaptureForm(false);
  }

  return (
    <div
      className={
        classNames(styles.pokemonInfoCard, {
          [styles.squareCornersBottom]: props.squareCornersBottom,
          [styles.expanded]: props.expanded
        })
      }
    >
      <div
        className={
          classNames(
            styles.imageBackground,
            styles[props.pokemon.types[0].type.name]
          )
        }
      >
        <img
          key={ props.pokemon.id }
          className={ styles.image }
          src={ props.pokemon.sprites.other['official-artwork'].front_default }
        />
        <div className={ styles.name }>
          <PokemonName
            pokemon={ props.pokemon }
          />
        </div>
      </div>
      <div className={ styles.details }>
        <div className={ styles.card }>
          <div className={ styles.title }>
            { 'About' }
          </div>
          <div className={ styles.row }>
            <div className={ styles.label }>
              { 'Type(s)' }
            </div>
            <div className={ styles.values }>
              {
                _.map(props.pokemon.types, ({ type }) => (
                  <div
                    key={ type.name }
                    className={ styles.value }
                  >
                    { _.capitalize(type.name) }
                  </div>
                ))
              }
            </div>
          </div>
          <div className={ styles.row }>
            <div className={ styles.label }>
              { 'Weight' }
            </div>
            <div className={ styles.values }>
              { `${props.pokemon.weight / 10} kg` }
            </div>
          </div>
          <div className={ styles.row }>
            <div className={ styles.label }>
              { 'Height' }
            </div>
            <div className={ styles.values }>
              { `${props.pokemon.height / 10} m` }
            </div>
          </div>
        </div>
        <div className={ styles.card }>
          <div className={ styles.title }>
            { 'Base Stats' }
          </div>
          {
            _.map(props.pokemon.stats, ({ stat, base_stat }) => (
              <div
                key={ stat.name }
                className={ styles.row }
              >
                <div className={ styles.label }>
                  { BASE_STAT_LABELS[stat.name] }
                </div>
                <div className={ styles.values }>
                  { base_stat }
                </div>
              </div>
            ))
          }
        </div>
        {
          capturedPokemon && (
            <div className={ styles.card }>
              <div className={ styles.title }>
                { 'Capture Information' }
              </div>
              {
                capturedPokemon.nickname && (
                  <div
                    className={ styles.row }
                  >
                    <div className={ styles.label }>
                      { 'Nickname' }
                    </div>
                    <div className={ styles.values }>
                      { capturedPokemon.nickname }
                    </div>
                  </div>
                )
              }
              <div
                className={ styles.row }
              >
                <div className={ styles.label }>
                  { 'Captured On' }
                </div>
                <div className={ styles.values }>
                  <FormattedDate
                    date={ capturedPokemon.capturedDate }
                  />
                </div>
              </div>
              <div
                className={ styles.row }
              >
                <div className={ styles.label }>
                  { 'Captured Level' }
                </div>
                <div className={ styles.values }>
                  { capturedPokemon.capturedLevel }
                </div>
              </div>
            </div>
          )
        }
      </div>
      {
        !capturedPokemon && (
          <div className={ styles.footer }>
            <Button
              grow
              primary
              onClick={ () => setShowCaptureForm(true) }
            >
              { 'Capture' }
            </Button>
            {
              showCaptureForm && (
                <Modal
                  close={ () => setShowCaptureForm(false) }
                >
                  <PokemonCaptureForm
                    pokemon={ props.pokemon }
                    onCapture={ capturePokemon }
                  />
                </Modal>
              )
            }
          </div>
        )
      }
    </div>
  );
};

PokemonInfoCard.propTypes = {
  capturedPokemons: PropTypes.arrayOf(
    CapturedPokemonPropType
  ).isRequired,
  pokemon: PokemonPropType.isRequired,
  expanded: PropTypes.bool,
  squareCornersBottom: PropTypes.bool,
  onCapture: PropTypes.func
};

export default connect(
  (state) => ({
    capturedPokemons: state.capturedPokemons.all
  })
)(PokemonInfoCard);
