import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import BreakpointsProvider from 'lib/components/providers/breakpoints/BreakpointsProvider';

import { loadCapturedPokemons } from 'app/store/actions/capturedPokemons';
import PokemonIndexPage from 'app/components/pokemon/PokemonIndexPage';
import CapturedPokemonIndexPage from 'app/components/pokemon/CapturedPokemonIndexPage';

import './app.scss';

export const ANIMATION_DELAY = 1100;

const BREAKPOINTS_CONFIG = {
  mobile: 50
};

const App = (props) => {
  useEffect(() => {
    props.loadCapturedPokemons();
  }, []);

  return (
    <BreakpointsProvider config={ BREAKPOINTS_CONFIG }>
      <BrowserRouter>
        <Routes>
          <Route
            key="/captured"
            exact
            path="/captured"
            element={ <CapturedPokemonIndexPage /> }
          />
          <Route
            key="/"
            exact
            path="/"
            element={ <PokemonIndexPage /> }
          />
        </Routes>
      </BrowserRouter>
    </BreakpointsProvider>
  );
};

App.propTypes = {
  loadCapturedPokemons: PropTypes.func.isRequired
};

export default connect(
  null,
  {
    loadCapturedPokemons
  }
)(App);
