import classNames from 'classnames';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';

const { Provider, Consumer } = React.createContext();

import styles from './breakpoints.scss';

class BreakpointsProvider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sortedBreakpoints: this.getSortedBreakpoints(),
      breakpoints: null
    };
  }

  componentDidMount() {
    window.addEventListener('resize', this.update);
    this.setState({ breakpoints: this.getBreakpoints() });
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.update);
  }

  update = _.throttle(() => {
    const breakpoints = this.getBreakpoints();
    if (!_.isEqual(breakpoints, this.state.breakpoints)) {
      this.setState({ breakpoints });
    }
  }, 50)

  getSortedBreakpoints() {
    return _.sortBy(
      _.map(this.props.config, (maxWidthRem, key) => ({
        maxWidthRem,
        key
      })),
      'maxWidthRem'
    );
  }

  getBreakpoints() {
    const fontSize = parseFloat(getComputedStyle(document.querySelector('body'))['font-size']);
    const rem = window.innerWidth / fontSize;

    const breakpoints = {};
    _.forEach(this.state.sortedBreakpoints, ({ maxWidthRem, key }) => {
      breakpoints[key] = rem < maxWidthRem;
    });

    return breakpoints;
  }

  render() {
    if (!this.state.breakpoints) {
      return null;
    }

    return (
      <div className={ classNames(styles.breakpoints, this.state.breakpoints) }>
        <Provider value={ this.state.breakpoints }>
          { this.props.children }
        </Provider>
      </div>
    );
  }
}

BreakpointsProvider.propTypes = {
  config: PropTypes.object.isRequired,
  children: PropTypes.node
};

export const withBreakpoints = (Component) => {
  const WithBreakpoints = React.forwardRef((props, ref) => (
    <Consumer>
      {
        (breakpoints) => (
          <Component
            { ...props }
            ref={ ref }
            breakpoints={ breakpoints }
          />
        )
      }
    </Consumer>
  ));

  WithBreakpoints.displayName = `WithBreakpoints(${Component.displayName || Component.name})`;
  return WithBreakpoints;
};

export default BreakpointsProvider;
